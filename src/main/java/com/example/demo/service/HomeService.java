package com.example.demo.service;

import com.example.demo.model.Product;

import java.util.Collection;

public interface HomeService {
//    List<User> getUsers();

    public void postProduct( Product newProduct);

    public void putProduct(String id, Product newProduct);

    public void deleteProduct(String id);

    public Collection<Product> getProduct();
}
