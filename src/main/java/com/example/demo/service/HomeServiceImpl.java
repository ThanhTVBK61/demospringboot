package com.example.demo.service;


import com.example.demo.common.exception.ProductNotFound;
import com.example.demo.model.Product;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class HomeServiceImpl implements HomeService{

//    private final HomeRepository homeRepository;
//
//    @Autowired
//    public HomeServiceImpl(HomeRepository homeRepository){
//        this.homeRepository = homeRepository;
//    }
//
//    @Override
//    public List<User> getUsers() {
//        return homeRepository.findAll();
//    }

    private static Map<String, Product> productRepo = new HashMap<>();
    static {
        Product honey = new Product();
        honey.setId("1");
        honey.setName("Honey");
        productRepo.put(honey.getId(), honey);

        Product almond = new Product();
        almond.setId("2");
        almond.setName("Almond");
        productRepo.put(almond.getId(), almond);
    }


    @Override
    public void postProduct(Product newProduct) {
        productRepo.put(newProduct.getId(), newProduct);
    }

    @Override
    public void putProduct(String id, Product newProduct) {
        productRepo.remove(id);
        newProduct.setId(id);
        productRepo.put(id, newProduct);
    }

    @Override
    public void deleteProduct(String id) {
        if(!productRepo.containsKey(id)) throw new ProductNotFound();
        productRepo.remove(id);
    }

    @Override
    public Collection<Product> getProduct() {
        return productRepo.values();
    }
}
