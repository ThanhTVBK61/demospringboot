package com.example.demo.controller;

import com.example.demo.model.Product;
import com.example.demo.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {
    private HomeService homeService;
    @Autowired
    public HomeController(HomeService homeService){
        this.homeService = homeService;
    }

//    @RequestMapping(value = "homepage", method = RequestMethod.POST)
//    public ResponseEntity getHomePage(){
//        return ResponseEntity.ok().body(homeService.getUsers());
//    }

    @RequestMapping(value = "/getProduct")
    ResponseEntity getProduct(){
        return new ResponseEntity<>(homeService.getProduct(), HttpStatus.OK);
    }

    @RequestMapping(value = "/putProduct/{id}", method = RequestMethod.PUT)
    ResponseEntity putProduct(@PathVariable("id") String id, @RequestBody() Product newProduct){
        homeService.putProduct(id, newProduct);
        return new ResponseEntity<>("Product is changed successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/postProduct", method = RequestMethod.POST)
    ResponseEntity postProduct(@RequestBody() Product newProduct){
        homeService.postProduct(newProduct);
        return new ResponseEntity<>("Product is created successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteProduct/{id}", method = RequestMethod.DELETE)
    ResponseEntity deleteProduct(@PathVariable() String id){

        homeService.deleteProduct(id);
        return new ResponseEntity<>("Product is deleted successfully", HttpStatus.OK);
    }
}
